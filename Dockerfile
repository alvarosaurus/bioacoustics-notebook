FROM jupyter/base-notebook:notebook-6.0.0

USER root

# install system libs and utils
RUN apt-get update && \
apt install -y apt-transport-https software-properties-common && \
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9 && \
add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/' && \
apt-get update && apt-get install -yq --no-install-recommends \
r-base-dev \
libsndfile1-dev \
libsbsms-dev \
libfftw3-dev \
sox \
libsox-fmt-mp3 \
nano \
libssl-dev \
libcurl4-openssl-dev \
libxml2-dev \
git

# install R kernel for Jupyter notebook
RUN \
Rscript -e "install.packages('devtools')"; \
Rscript -e "devtools::install_github('IRkernel/IRkernel')"; \
Rscript -e "IRkernel::installspec()";

# install R dev, sound and plotting packages
RUN \
Rscript -e "install.packages('warbleR')"; \
Rscript -e "install.packages('sound')"; \
Rscript -e "install.packages('tuneR')"; \
Rscript -e "install.packages('seewave')"; \
Rscript -e "install.packages('ggplot2')"; \
Rscript -e "install.packages('cowplot')"; \
Rscript -e "install.packages('boot')";
Rscript -e "install.packages('suncalc')";
Rscript -e "install.packages('gridExtra')";

# install packages for calling APIs
Rscript -e "install.packages('httr')";
Rscript -e "install.packages('jsonlite')";

# Install Python packages
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
