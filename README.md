A Jupyter notebook for bioacoustic analysis.

# Get from Docker Hub
A ready container is provided on docker hub. Get it on https://hub.docker.com/r/aot29/bioacoutics-notebook
The present source code is provided if you wish to build the container yourself, or wish to enhance it.

# Get Docker
This is the source code for a Docker container.
You will need to install Docker on your machine to run it. Get Docker at: https://www.docker.com/get-started

# Building the container
1. Clone the repo
2. `docker build .` will build the container and run it

# Usage
To use this container in your own project, include it in your docker-compose file, e.g.
```
version: '3'
services:
   bioacoustics-notebook:
      image: aot29/bioacoustics-notebook:latest
      container_name: bioacoustics-notebook
      ports:
        - "8888:8888"
      volumes:
        - "./:/home/jovyan"
      tty: true
```
(if you build it yourself, you might want to change the name of the image).
Copy the `install` and `start` scripts from this repo to your projects repo, and run these scripts.

# Examples
1. Install the R kernel: `./install`
2. Start the notebook server: `./start`, go to the given URL in a browser.
3. Open the example notebook `Examples.ipynb`

# References
* https://hub.docker.com/r/jupyter/minimal-notebook
* https://jupyter-docker-stacks.readthedocs.io/en/latest/index.html
* https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#jupyter-minimal-notebook
* https://www.dataquest.io/blog/docker-data-science/
